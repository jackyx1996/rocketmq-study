/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.apache.rocketmq.client.producer;

import org.apache.rocketmq.client.exception.MQClientException;
import org.apache.rocketmq.common.message.Message;
import org.apache.rocketmq.remoting.RPCHook;

import java.util.concurrent.ExecutorService;

/**
 * 事务消息的发送过程分为两个阶段：
 * 第一阶段：发送事务消息
 * 第二阶段：发送endTransaction消息
 * RocketMQ 4.3.0才开始全面支持事务消息
 */
public class TransactionMQProducer extends DefaultMQProducer {
    // 事务监听器, 主要功能是执行本地事务和执行事务回查
    private TransactionListener transactionListener;

    // Broker回查请求处理的线程池
    private ExecutorService executorService;

    public TransactionMQProducer() {
    }

    public TransactionMQProducer(final String producerGroup) {
        super(producerGroup);
    }

    public TransactionMQProducer(final String producerGroup, RPCHook rpcHook) {
        super(producerGroup, rpcHook);
    }

    /**
     * 启动事务消息生产者
     */
    @Override
    public void start() throws MQClientException {
        // 与正常的启动方法不同, 增加initTransactionEnv的方法, 即增加的初始化事务消息的环境信息
        this.defaultMQProducerImpl.initTransactionEnv();
        super.start();
    }

    /**
     * 关闭生产者, 回收生产者资源。
     * 该方法是启动方法的逆向过程, 功能是关闭生产者、销毁事务环境。销毁事务环境是指销毁事务回查线程池, 清除回查任务队列
     */
    @Override
    public void shutdown() {
        super.shutdown();
        this.defaultMQProducerImpl.destroyTransactionEnv();
    }

    /**
     * 发送事务消息
     */
    @Override
    public TransactionSendResult sendMessageInTransaction(final Message msg, final Object arg) throws MQClientException {
        if (null == this.transactionListener) {
            throw new MQClientException("TransactionListener is null", null);
        }

        return this.defaultMQProducerImpl.sendMessageInTransaction(msg, transactionListener, arg);
    }

    public TransactionListener getTransactionListener() {
        return transactionListener;
    }

    public void setTransactionListener(TransactionListener transactionListener) {
        this.transactionListener = transactionListener;
    }

    public ExecutorService getExecutorService() {
        return executorService;
    }

    public void setExecutorService(ExecutorService executorService) {
        this.executorService = executorService;
    }
}
