/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.apache.rocketmq.common.consumer;

public enum ConsumeFromWhere {
    // 从上次消费的位点开始消费, 相当于断点继续
    CONSUME_FROM_LAST_OFFSET,

    // 下面标了过期注解的已经不支持了, 实现和FROM_LAST_OFFSET一致
    @Deprecated
    CONSUME_FROM_LAST_OFFSET_AND_FROM_MIN_WHEN_BOOT_FIRST,
    @Deprecated
    CONSUME_FROM_MIN_OFFSET,
    @Deprecated
    CONSUME_FROM_MAX_OFFSET,
    // 从ConsumerQueue的最小位点开始消费, 相当于全部重新消费了
    CONSUME_FROM_FIRST_OFFSET,
    // 从指定的时间点开始消费
    CONSUME_FROM_TIMESTAMP,
}
